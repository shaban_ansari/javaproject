# Use an official Tomcat image as a parent image
FROM tomcat:9.0-jdk17-openjdk-slim

# Remove the default webapps to avoid conflicts
RUN rm -rf /usr/local/tomcat/webapps/*

# Copy the WAR file to the webapps directory
COPY target/java-0.0.1-SNAPSHOT.war.original /usr/local/tomcat/webapps/ROOT.war

# Change Tomcat's default port from 8080 to 8090
RUN sed -i 's/port="8080"/port="8090"/g' /usr/local/tomcat/conf/server.xml

# Expose port 8090
EXPOSE 8090

# Start Tomcat
CMD ["catalina.sh", "run"]
