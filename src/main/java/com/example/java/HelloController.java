package com.example.java;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello() {
        // Get the port from the environment variable
        String port = System.getProperty("server.port", "8085"); // default to 8085 if not set
        System.out.println("Port: " + port); // Print the port to the console
        return "Hello, World! You are accessing this on port: " + port;
    }
}
